function path_planner(cond_filepath, traj_filepath)
  
  is_debug = false;
  logging = false;
  
  try
  while true
    timestamp_str = datetime('now', 'Format', 'yyyy-MM-dd_HH-mm-ss');
    
    % Load robot
    fprintf('Loading robot...');
    addpath('./utils');
    ini = load_config();
    robot = class_robot(ini);
    num_joints = size(robot.myjoints, 1);
    fprintf('done!\n');
    
    fprintf('Joint condition:\n');
    disp(robot.myjoints);
    
    
    % wait for cond_filepath ----------------------------------------------------------------
    fprintf('wait for condition-file');
    while exist(cond_filepath) ~= 2
      fprintf('.');
      pause(0.1);
    end
    fprintf('done!\n');
    
    % load condition file ----------------------------------------------------------------
    fprintf('Loading condition...');
    if exist(cond_filepath) ~= 2 
      fprintf('can''t find the condition file!\n'); 
      continue; 
    end
    
    pf = fopen(cond_filepath);
    data = fscanf(pf, '%f');
    fclose(pf);
    %delete cond_filepath;
    if length(data) ~= num_joints + 14
      msg = sprintf('Unable to load the condition file! It may has illegal format.\nFile path: %s', cond_filepath);
      error(msg);
    end
    fprintf('done!\n');
    
    % logged
    if(logging)
        cond_filepath_log = sprintf('./log/log_%s_%s', timestamp_str, cond_filepath);
        result = system(sprintf('cp %s %s', cond_filepath, cond_filepath_log));
        if result == 0
          fprintf('Condition-file is logged as ''%s''.\n', cond_filepath_log);
        else
          fprintf('--------------------------------------------------------------------\n');
          fprintf('Can''t creat the log-file of ''%s''.\n', cond_filepath);
          fprintf('--------------------------------------------------------------------\n');
        end
    end
    % set condition struct ---------------------------------------------------------------
    fprintf('Parsing condition...');
    cond.q = zeros(22, 1);
    cond.q(:) = data(1:(num_joints));
    hand_quat = data((num_joints + 1):(num_joints + 4));
    cond.hand_rot = quat_DCM(hand_quat);
    cond.goal_pos = data((num_joints + 5):(num_joints + 7));
    cond.shelf_quat = data((num_joints + 8):(num_joints + 11));
    cond.shelf_rot = quat_DCM(cond.shelf_quat);
    cond.shelf_pos = data((num_joints + 12):(num_joints + 14));
    [~, pos] = robot.get_link_state(cond.q);
    cond.start_pos = pos;
    fprintf('done!\n');
    
    % hard code for position of shelf / Norikazu sugimoto / 19, Mar. 2020
    cond.shelf_pos = [612; -786; -826]./1000;
    
    % Load shelf ------------------------------------------------------------
    fprintf('Loading shelf...'); % Load shelf
    shelf = class_shelf();
    shelf.move(cond.shelf_pos, cond.shelf_rot);
    fprintf('done!\n');
    
    % Load reachability-map ------------------------------------------------------------
    fprintf('Loading reachability-map...');
    filepath = sprintf('autogen_%s_reachability_map.mat', data_hash(ini));
    load(filepath);
    fprintf('done!\n');
    
    % transfer & rotate robot-base ------------------------------------------------------------
    rot_base = Angles321_DCM([0, 0, 0]./180*pi);
    pos_base = [0; 0; 0];
    
    robot.rot_base = rot_base;
    robot.pos_base = pos_base;
    
    for n = 1:length(reachability_map)
      reachability_map{n}.p = rot_base*reachability_map{n}.p + pos_base;
    end
    
    cond.start_pos = rot_base*cond.start_pos + pos_base;
    cond.goal_pos = rot_base*cond.goal_pos + pos_base;
    
    
    % collision checking -----------------------------------------------------------------
    fprintf('Collision checking...');
    issafe = zeros(1, length(reachability_map));
    for n = 1:length(reachability_map)
      segs = mksegment_for_collision(reachability_map{n}.p);
      issafe(n) = shelf.collision_check(segs);
    end
    fprintf('done!\n');
    
    
    % plot --------------------
    if is_debug == true
      % plot robot & shelf
      myfigure(1); clf;
      robot.plot_robot(cond.q);
      shelf.plot();
      for n = 1:length(reachability_map)
        if issafe(n) == true
          plot3(reachability_map{n}.p(1), reachability_map{n}.p(2), reachability_map{n}.p(3), 'r.');
        end
      end
      grid('on');
    end
    
    
    % find nearest-point of start and goal positions ----------------------------------------
    centers = zeros(3, length(reachability_map));
    for n = 1:length(reachability_map)
      centers(:, n) = reachability_map{n}.p;
    end
    [start_err, start_idx] = min(sum((centers - repmat(cond.start_pos, 1, length(reachability_map))).^2));
    [goal_err, goal_idx] = min(sum((centers - repmat(cond.goal_pos, 1, length(reachability_map))).^2));
    shreshold_d2 = 0.05^2;
    isok_start = start_err <= shreshold_d2;
    isok_goal = goal_err <= shreshold_d2;
    if isok_start == false || isok_goal == false
      % error!!!
      if isok_start == false && isok_goal == false
        msg = sprintf('Failed! Both of start and goal postion are exceeded reachable area. \n');
      elseif isok_start == false
        msg = sprintf('Failed! Start postion are exceeded reachable area. \n');
      else
        msg = sprintf('Failed! Goal postion are exceeded reachable area. \n');
      end
      pf = fopen(traj_filepath, 'w'); 
      if pf < 0
        msg2 = sprintf('Unable to open: %s', traj_filepath);
        fprintf(msg2);
      end
      fprintf(msg);
      fprintf(pf, '%c', '!');
      if is_debug == true % plot start & goal positions
        myfigure(1);
        %for n = 1:length(reachability_map)
        %  plot3(reachability_map{n}.p(1), reachability_map{n}.p(2), reachability_map{n}.p(3), 'ro'); hold('on');
        %end
        plot_sphere(cond.start_pos, 0.01, 'blue');
        plot_sphere(cond.goal_pos, 0.04, 'blue');
        axis('equal');
        xlabel('x [m]'); ylabel('y [m]'); zlabel('z [m]');
        
        fprintf('debug mode.');
        pause
      end
      continue;
    end
    
    % path-planning ----------------------------------------------------------------------------
    fprintf('path-planning...');
    isfind = false;
    ignore_idx = find(issafe == 0);
    [path_idx, path] = astar(reachability_map, start_idx, goal_idx, ignore_idx); % here, path is x-y-z.
    
    if ~(isempty(path_idx) == true | isempty(path) == true)
      isfind = true;
      path = fliplr(path);
      err_start_pos = sum((path(:, 1)   - cond.start_pos).^2);
      err_goal_pos  = sum((path(:, end) - cond.goal_pos).^2);
    end
    
    if isfind == false
      msg = sprintf('Failed! Unable to reach. There is no path to the goal.');
      error(msg);
    else
      fprintf('done.\n');
      %msg = sprintf('Find reachable path successfully.');
      path(:, 1)   = cond.start_pos;
      path(:, end) = cond.goal_pos;
      cond.path = path;
      
      if is_debug == true % plot start & goal positions
        myfigure(1);
        plot3(path(1, :), path(2, :), path(3, :), 'b+-', 'LineWidth', 2); hold('on');
        plot_sphere(cond.start_pos, 0.01, 'blue');
        plot_sphere(cond.goal_pos, 0.02, 'blue');
        robot.plot_robot(cond.q);
        axis('equal');
        xlabel('x [m]'); ylabel('y [m]'); zlabel('z [m]');
      end
    end
    
    % ik ---------------------------------------------------------------------
    num = size(path, 2);
    q = cond.q;
    qd = zeros(size(robot.myjoints, 1), 1);
    cond.path_RARM_A5_X = zeros(4 + 3, num); % quat + pos
    fprintf('Solving ik');
    for n = 1:num
      fprintf('.');
      aa_des = rot2aa(quat_DCM(ini.reachability.quat_des));
      cond_ik.state_des = [aa_des; cond.path(:, n)];
      cond_ik.q = q;
      cond_ik.qd = qd;
      cond_ik.target_link_id = robot.mylinks{ini.target_link_name, 'id'};
      cond_ik.target_qid = robot.myjoints{ini.target_joint_names, 'qid'};
      [q, errors, iter] = ik(robot, cond_ik);
      [rot, pos] = robot.get_link_state_by_name(q, 'RARM_A5_X');
      qurt = DCM_quat(rot);
      cond.path_RARM_A5_X(:, n) = [qurt; pos];
    end
    fprintf('done!\n');
    
% $$$     num = size(path, 2);
% $$$     q = zeros(size(robot.myjoints, 1), 1);
% $$$     [rot_ab, pos_ab] = robot.get_link_state_relative(q, 'RARM_A5_X', 'suction_pad');
% $$$     rot_suction = quat_DCM(ini.reachability.quat_des);
% $$$     cond.path_RARM_A5_X = zeros(4 + 3, num); % quat + pos
% $$$     fprintf('Calculating state of RARM_A5_X');
% $$$     for n = 1:num
% $$$       fprintf('.');
% $$$       rot_RARM_A5_X = rot_suction*inv(rot_ab);
% $$$       pos_RARM_A5_X = cond.path(:, n) - pos_ab;
% $$$       qurt_RARM_A5_X = DCM_quat(rot_RARM_A5_X);
% $$$       cond.path_RARM_A5_X(:, n) = [qurt_RARM_A5_X; pos_RARM_A5_X];
% $$$     end
    
    % generate traj. ---------------------------------------------------------------------
    w = 0.01; % should be less than 0.01
    path_interp = interp_phase(cond.path_RARM_A5_X, w);
    path_length = sum(sqrt(sum((diff(path_interp')').^2)));
    
    dt = 0.1; % hard coding...
    T = 5; % hard coding...
    t = [0:dt:T];
    v = (path_length/T)*(30*(t/T).^2 - 60*(t/T).^3 + 30*(t/T).^4); % travel vel.
    p = path_length*(10*(t/T).^3 - 15*(t/T).^4 + 6*(t/T).^5); % travel distance
    
    %
    traj = zeros(4 + 3, length(t)); % quat + pos
    for s = 1:length(t)
      idx1 = min(length(path_interp) - 1, floor(p(s)/w) + 1);
      alpha = p(s)/w - floor(p(s)/w);
      interp_pos = path_interp(:, idx1)*(1 - alpha) + path_interp(:, idx1 + 1)*alpha;
      
      %pos_traj(1:3, s) = DCM_Angles321(cond.hand_rot);
      traj(:, s) = interp_pos;
    end
    smoothness = 0.5;
    cond.traj_filt = smooth_gaussian(traj, max(1, floor(smoothness/dt)));
    
    if is_debug == true % plot results
      myfigure(1);
      plot3(cond.traj_filt(5, :), cond.traj_filt(6, :), cond.traj_filt(7, :), 'r-', 'LineWidth', 3);
      
      myfigure(2); clf;
      subplot(2, 1, 1); % traj of quat
      plot(t, traj(1:4, :)', 'LineWidth', 2);
      legend({'quat-x', 'quat-y', 'quat-z', 'quat-th'});
      set(gca, 'XLim', [0, max(t)]);
      xlabel('t [s]');
      ylabel('quat.');
      subplot(2, 1, 2); % traj of pos
      plot(t, traj(5:7, :)', 'LineWidth', 2);
      set(gca, 'XLim', [0, max(t)]);
      legend({'x', 'y', 'z'});
      xlabel('t [s]');
      ylabel('Position [m]');
    end
    
    
    % Save resulted traj. --------------------------------------------------------------
    hand_quat = DCM_quat(cond.hand_rot);
    pf = fopen(traj_filepath, 'w');
    if pf < 0
      msg = sprintf('Unable to open: %s', traj_filepath);
      error(msg);
    end
    for s = 1:length(t)
      fprintf(pf, '%f', t(s));
      for d = 1:4 % Quat.
        fprintf(pf, ' %f', cond.traj_filt(d, s));
      end
      for d = 5:7 % Position
        fprintf(pf, ' %f', cond.traj_filt(d, s) - pos_base(d - 4));
      end
      
      
      fprintf(pf, '\n');
    end
    fclose(pf);
    
    % logged
    if(logging)
        traj_filepath_log = sprintf('./log/log_%s_%s', timestamp_str, traj_filepath);
        result = system(sprintf('cp %s %s', traj_filepath, traj_filepath_log))
        if result == 0
          fprintf('Trajectory-file is logged as ''%s''.\n', traj_filepath_log);
        else
          fprintf('--------------------------------------------------------------------\n');
          fprintf('Can''t creat the log-file of ''%s''.\n', traj_filepath);
          fprintf('--------------------------------------------------------------------\n');
        end
    end
    %
    if is_debug == true
      fprintf('\n');
      fprintf('===================================\n');
      fprintf('  This is debug mode, push enter.\n');
      fprintf('===================================\n');
      pause
    end
  end
  catch ME
    disp(ME.message);
    if(logging)
        filepath = sprintf('./log/log_%s_variables.mat', timestamp_str);
        save(filepath);
        fprintf('--------------------------------------------------------------------\n');
        fprintf('Error is occured.\n');
        fprintf('Current variavles are logged as ''%s''.\n', filepath);
        fprintf('--------------------------------------------------------------------\n');
    end
  end
end

function segs = mksegment_for_collision(p)
  r = 0.05; % radius of bottle
  h = 0.10; % hight of bottle
  segs{1} = [p, p + [ r;  0;  0.0]];
  segs{2} = [p, p + [-r;  0;  0.0]];
  segs{3} = [p, p + [ 0;  r;  0.0]];
  segs{4} = [p, p + [ 0; -r;  0.0]];
  segs{5} = [p, p + [ 0;  0;  r]];
  segs{6} = [p, p + [ 0;  0; -h]];
end


