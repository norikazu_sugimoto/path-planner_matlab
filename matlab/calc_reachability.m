function calc_reachability
  addpath('./utils');
  ini = load_config();
  fprintf('data_hash(ini): %s\n', data_hash(ini));
  
  % eval reachability
  filepath = sprintf('autogen_%s_calc_reachability_precomp.mat', data_hash(ini));
  if exist(filepath) == 2
    load(filepath);
  else
    [isdone, errors, angles, centers] = reaching(ini);
    save(filepath, 'isdone', 'errors', 'angles', 'centers');
  end
  
  
  % make map and save
  filepath = sprintf('autogen_%s_reachability_map.mat', data_hash(ini));
  reachability_map = make_map(ini, isdone, errors, angles, centers);
  save(filepath, 'reachability_map');
end
  
function map = make_map(ini, isdone, errors, angles, centers)
  % make reachability-map
  bias_list = ini.reachability.bin.* ...
      [[-1, -1, -1]', [-1, -1,  0]', [-1, -1,  1]', [-1,  0, -1]', [-1,  0,  0]', [-1,  0,  1]', [-1,  1, -1]', [-1,  1,  0]', [-1,  1,  1]', ...
       [ 0, -1, -1]', [ 0, -1,  0]', [ 0, -1,  1]', [ 0,  0, -1]',                [ 0,  0,  1]', [ 0,  1, -1]', [ 0,  1,  0]', [ 0,  1,  1]', ...
       [ 1, -1, -1]', [ 1, -1,  0]', [ 1, -1,  1]', [ 1,  0, -1]', [ 1,  0,  0]', [ 1,  0,  1]', [ 1,  1, -1]', [ 1,  1,  0]', [ 1,  1,  1]'];
  
  % extruct index of reachable state
  center_num = size(centers, 2);
  isreach = zeros(1, center_num);
  
  
  idx_list = find(errors(1, :) < 3/180*pi & errors(2, :) < 2.5*1e-2);
  %idx_list = find(errors(1, :) < 45/180*pi & errors(2, :) < 1e-1);
  
  isreach(idx_list) = 1;
  map = cell(1, length(idx_list));
  
  %
  for n = 1:length(idx_list)
    fprintf('making... %5d/%5d\r', n, length(idx_list));
    p = centers(:, idx_list(n));
    
    % find neighbor
    neighbor_list = [];
    for i = 1:size(bias_list, 2)
      tmp = p + bias_list(:, i);
      e = sum((centers - repmat(tmp, 1, center_num)).^2);
      [min_e, idx] = min(e);
      if min_e < 1e-8 && isreach(idx) == true
        tmp = find(idx_list == idx);
        neighbor_list = [neighbor_list, tmp];
      end
    end
    
    % add map struct
    map{n}.p = p;
    map{n}.q = angles(:, idx_list(n));
    map{n}.neighbor_list = neighbor_list;
  end
  fprintf('\n');
  
  % plot
  myfigure(1); clf;
  for n = 1:length(map)
    fprintf('plotting... %5d/%5d\r', n, length(idx_list));
    p = map{n}.p;
    l = [1, 1, 1].*ini.reachability.bin;
    plot_cube(l, p' - l/2, 0.1, 'r'); hold('on');
  end
  
  robot = class_robot(ini);
  q = zeros(size(robot.myjoints, 1));
  robot.plot_robot(q);
  
  axis('equal');
  fprintf('\n');
end

function [isdone, errors, angles, centers] = reaching(ini)
  % load robot
  robot = class_robot(ini);
  cond.target_link_id = robot.mylinks{ini.target_link_name, 'id'};
  cond.target_qid = robot.myjoints{ini.target_joint_names, 'qid'};
  
  
  % set initial pusture
  robot.myjoints{'RARM_A3_Y_JOINT', 'q'} = -90/180*pi;
  cond.q = robot.myjoints.q;
  cond.qd = robot.myjoints.qd;
  %disp(cond.q)
  [rot_init, pos_init] = robot.get_link_state(cond.q);
  
  
  % set desired state
  aa_des = rot2aa(ini.reachability.rot_des);
  
  %
  xlist = [ 0.0:ini.reachability.bin:0.6];
  ylist = [-0.8:ini.reachability.bin:0.0];
  zlist = [-0.2:ini.reachability.bin:0.6];
  %xlist = [-1.5:ini.reachability.bin:1.5];
  %ylist = [-1.5:ini.reachability.bin:1.5];
  %zlist = [-0.5:ini.reachability.bin:1.5];
  
  center_num = length(xlist)*length(ylist)*length(zlist);
  centers = zeros(3, center_num);
  
  num_joints = size(robot.myjoints, 1);
  isdone = zeros(1, center_num);
  errors = zeros(2, center_num);
  angles = zeros(num_joints, center_num);
  
  %
  myfigure(100); clf;
  start_time = tic;
  fprintf('\n');
  counter = 0;
  for nx = 1:length(xlist)
    for ny = 1:length(ylist)
      for nz = 1:length(zlist)
        counter = counter + 1;
        centers(:, counter) = [xlist(nx), ylist(ny), zlist(nz)]' + ini.pos_base;
        cond.state_des = [aa_des; centers(:, counter)];
        if centers(1, counter) < 0 && centers(2, counter) > 0
          isdone(counter) = 1;
          errors(:, counter) = Inf.*ones(2, 1);
          angles(:, counter) = cond.q;
        else
          [q, error, iter] = ik(robot, cond);
          isdone(counter) = 1;
          errors(:, counter) = error;
          angles(:, counter) = q;
        end
        
        %
        pp = counter/center_num;
        elapsed_time = toc(start_time);
        remaining_time = (1 - pp)*elapsed_time/pp;
        fprintf('\r%8.3f%% complete, %10.4fh passed, %10.4fh remaining(estimated).', pp*100, elapsed_time/3600, remaining_time/3600);
        
        % plot
        myfigure(100);
        if errors(1, counter) < 2/180*pi & errors(2, counter) < 1e-2; ps = 'ro';
        else;                                                         ps = 'k.'; end
        plot3(centers(1, counter), centers(2, counter), centers(3, counter), ps); hold('on');
        axis('equal');
        grid('on');
        drawnow();
      end
    end
  end
  
end

