function make_fk_func
  addpath('./utils');
  [ini, hash] = load_config();
  fprintf('data_hash(ini): %s\n', hash);
  
  % load robot
  ini
  robot = class_robot(ini);
  target_link_id = robot.mylinks{ini.target_link_name, 'id'};
  
  % Base-link orientation
  Euler_Ang = sym('Euler_Ang', [3, 1], 'real');
  rot0 = Angles321_DCM(Euler_Ang)';
  % Base-link position
  pos0 = sym('pos0', [3, 1], 'real');
  % Joint displacements
  q = sym('q', [size(robot.myjoints, 1), 1], 'real');
  
  % set isuse or not, and make cmd for qset
  isuse = zeros(length(q), 1);
  qset_cmd = '';
  arg_str = '';
  for n = 1:length(q)
    for m = 1:length(ini.target_joint_names)
      if strcmp(robot.myjoints.Row{n}, ini.target_joint_names{m}) == true
        isuse(n) = 1;
        
        %qset_cmd = sprintf('%s q%d = robot.myjoints{''%s'', ''q''};', qset_cmd, n, robot.myjoints.Row{n});
        qset_cmd = sprintf('%s q%d = q(%d);', qset_cmd, n, n);
        
        arg_str = sprintf('%s, q%d', arg_str, n);
        
      end
    end
  end
  qset_cmd = qset_cmd(2:end);
  arg_str = arg_str(3:end);
  tmp_str = sprintf('[rot, pos, J] = autogen_%s_get_link_state', data_hash(ini));
  qset_cmd = sprintf('%s\n%s(%s);', qset_cmd, tmp_str, arg_str);
  
  
  filepath = sprintf('autogen_%s_qset.m', data_hash(ini));
  pf = fopen(filepath, 'w');
  fprintf(pf, '%s', qset_cmd);
  fclose(pf);
  
  
  %
  precomp_filepath = sprintf('autogen_%s_make_fk_func_precomp.mat', data_hash(ini));
  if exist(precomp_filepath) == 2
    fprintf('make_fk_func: Loading pre-computed results...');
    load(precomp_filepath);
    fprintf('done.\n');
  else
    fprintf('make_fk_func: pre-computing kinematics(1/3)...');
    stime = tic();
    [rot_joints, rot_links, pos_joints, pos_links, e, g] = Kinematics(rot0, pos0, q, robot.spart);
    rot_joints = combine(rot_joints, 'sincos'); fprintf('.');
    rot_links = combine(rot_links, 'sincos'); fprintf('.');
    pos_joints = combine(pos_joints, 'sincos'); fprintf('.');
    pos_links = combine(pos_links, 'sincos'); fprintf('.');
    e = combine(e, 'sincos'); fprintf('.');
    g = combine(g, 'sincos'); fprintf('.');
    fprintf('done, %f sec passed.\n', toc(stime));
    
    fprintf('make_fk_func: pre-computing diff-kinematics(2/3)...');
    stime = tic();
    [Bij, Bi0, P0, pm] = DiffKinematics(rot0, pos0, pos_links, e, g, robot.spart);
    Bij = combine(Bij, 'sincos'); fprintf('.');
    Bi0 = combine(Bi0, 'sincos'); fprintf('.');
    P0 = combine(P0, 'sincos'); fprintf('.');
    pm = combine(pm, 'sincos'); fprintf('.');
    fprintf('done, %f sec passed.\n', toc(stime));
    
    fprintf('make_fk_func: pre-computing Jacobians(3/3)...');
    stime = tic();
    [J0, J] = Jacob(pos_links(:, target_link_id), pos0, pos_links, P0, pm, target_link_id, robot.spart);
    J0 = combine(J0, 'sincos'); fprintf('.');
    J = combine(J, 'sincos'); fprintf('.');
    fprintf('done, %f sec passed.\n', toc(stime));
    
    fprintf('make_fk_func: saving pre-computing results...');
    stime = tic();
    save(precomp_filepath);
    fprintf('done, %f sec passed.\n', toc(stime));
  end
  
  
  % base
  Euler_Ang1 = 0;
  Euler_Ang2 = 0;
  Euler_Ang3 = 0;
  pos01 = 0;
  pos02 = 0;
  pos03 = 0;
  
  % joints outside of right-arm
  cmd_str = '';
  for n = 1:length(q)
    if isuse(n) == false
      cmd_str = sprintf('%s q%d=0;', cmd_str, n);
    end
  end
  eval(cmd_str);
  
  stime = tic();
  fprintf('make_fk_func: Simplifying...');
  %rot = simplify(combine(subs(rot_links(:, :, target_link_id)), 'sincos')); fprintf('.');
  %pos = simplify(combine(subs(pos_links(:, target_link_id)), 'sincos')); fprintf('.');
  %J   = simplify(combine(subs(J), 'sincos'));
  rot = combine(subs(rot_links(:, :, target_link_id)), 'sincos');
  pos = combine(subs(pos_links(:, target_link_id)), 'sincos');
  J = combine(subs(J), 'sincos');
  %rot = subs(rot_links(:, :, target_link_id));
  %pos = subs(pos_links(:, target_link_id));
  %J   = subs(J);
  fprintf('done, %f sec passed.\n', toc(stime));
  
  stime = tic();
  fprintf('make_fk_func: Writing function...');
  filepath = sprintf('autogen_%s_get_link_state.m', data_hash(ini));
  matlabFunction(rot, pos, J, 'file', filepath, 'Optimize', false);
  fprintf('done, %f sec passed.\n', toc(stime));
  
  %stime = tic();
  %fprintf('make_fk_func: Writing function...');
  %filepath = sprintf('autogen_%s_get_link_state_.m', data_hash(ini));
  %matlabFunction(rot, pos, J, 'file', filepath, 'Optimize', true);
  %fprintf('done, %f sec passed.\n', toc(stime));
  
  
end

