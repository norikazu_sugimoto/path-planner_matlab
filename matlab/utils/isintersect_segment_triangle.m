function [isintersect, x] = isintersect_segment_triangle(a, b, p1, p2, p3)
% input:
%   a, b: 線分の両端(3x1)
%   p1, p2, p3: 三角形の頂点(3x1)
% output:
%   isintersect: 三角形と線分が交わっていたらtrue、それ以外はfalse
%   x: 交わっていた場合はその交点座標、それ以外は空ベクトル

  isplot = false;
  X = 1; Y = 2; Z = 3;
  
  %a = 5*rand(3, 1);
  %b = 5*rand(3, 1);
  %p1 = 10*rand(3, 1);
  %p2 = 10*rand(3, 1);
  %p3 = 10*rand(3, 1);
  
  % Tomas M\"{o}llerのアルゴリズム
  e2 = p2 - p1;
  e3 = p3 - p1;
  v = b - a;
  r = a - p1;
  
  tmp = 1/(cross(v, e3)'*e2)*[cross(r, e2)'*e3; cross(-v, r)'*e3; cross(-v, e2)'*r];
  t = tmp(1);
  u = tmp(2);
  v = tmp(3);
  
  if (0 <= u && 0 <= v && u + v <=1) && (0 <= t && t <= 1)
    x = (1 - u - v)*p1 + u*p2 + v*p3;
    isintersect = true;
  else
    x = [];
    isintersect = false;
  end
  
  if isplot == true
    figure(1); clf;
    plot3([a(X), b(X)], [a(Y), b(Y)], [a(Z), b(Z)], 'r+-'); hold('on'); % 線分ab
    patch([p1(X), p2(X), p3(X)], [p1(Y), p2(Y), p3(Y)], [p1(Z), p2(Z), p3(Z)], 'c'); % 三角形(p1, p2, p3)
    if isintersect == true
      plot3(x(X), x(Y), x(Z), 'r.', 'MarkerSize', 10); % 線分と平面の交点
    end
    text(p1(X), p1(Y), p1(Z), '1');
    text(p2(X), p2(Y), p2(Z), '2');
    text(p3(X), p3(Y), p3(Z), '3');
    grid('on');
    axis('equal');
  end
end

