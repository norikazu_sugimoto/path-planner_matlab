classdef class_robot
  properties
    ini
    spart, spart_key
    myjoints, mylinks
    q, qd
    rot_base, pos_base
    is_precomp, qset_cmd
  end
  methods
    % constructor -----------------------------------
    function obj = class_robot(ini_, varargin)
    % Usage:
    %   obj = class_robot('TX07.urdf')
    %   obj = class_robot('TX07.urdf', 'base_rotation', eye(3))
    %   obj = class_robot('TX07.urdf', 'base_position', [0; 0; 0.4])
      
      % valid funcs
      %valid_fpath = @(x) ischar(x);
      %valid_rotation = @(x) (isnumeric(x) == true) && (size(x, 1) == 3) && (size(x, 2) == 3);
      %valid_position = @(x) (isnumeric(x) == true) && (size(x, 1) == 3) && (size(x, 2) == 1);
      
      % parsing
      %p = inputParser;
      %default_base_rotation = eye(3);
      %default_base_position = [0; 0; 0];
      %addRequired(p, 'urdf_filepath', valid_fpath);
      %addOptional(p, 'base_rotation', default_base_rotation, valid_rotation);
      %addOptional(p, 'base_position', default_base_position, valid_position);
      %parse(p, urdf_filepath, varargin{:});
      
      %obj.rot0 = p.Results.base_rotation; % rotation-matrix of base-link
      %obj.pos0 = p.Results.base_position; % position of base-link
      %obj.u0 = zeros(6, 1);
      
      
      obj.ini = ini_;
      [spart_, spart_key_] = urdf2robot(obj.ini.urdf_filepath);
      obj.spart = spart_;
      obj.spart_key = spart_key_;
      fprintf('class_robot: hash is %s\n', data_hash(obj.spart));
      
      % -----------------------------
      row_names = cell(obj.spart.n_q, 1);
      qid = zeros(obj.spart.n_q, 1);
      q_min = zeros(obj.spart.n_q, 1); q_max = zeros(obj.spart.n_q, 1);
      q = []; qd = [];
      for n = 1:length(obj.spart.joints)
        qid_ = obj.spart.joints(n).qid;
        if qid_ < 0; continue; end
        row_names{qid_, 1} = obj.spart.joints(n).name;
        qid(qid_, 1) = qid_;
        q_min(qid_, 1) = obj.spart.joints(n).limit(1);
        q_max(qid_, 1) = obj.spart.joints(n).limit(2);
        q(qid_, 1) = max(q_min(qid_), 0);
        qd(qid_, 1) = 0;
      end
      obj.myjoints = table(qid, q_min, q_max, q, qd, 'RowNames', row_names);
      
      % -----------------------------
      tmp = struct2cell(obj.spart.links);
      id = cell2mat(squeeze(tmp(2, 1, :)));
      obj.mylinks = table(id, 'RowNames', squeeze(tmp(4, 1, :)));
      
      %
      obj.rot_base = obj.ini.rot_base;
      obj.pos_base = obj.ini.pos_base;
      
      %
      obj.qset_cmd = sprintf('autogen_%s_qset', data_hash(obj.ini));
      %obj.get_link_state_cmd = sprintf('autogen_%s_get_link_state', data_hash(obj.ini));
      if exist([obj.qset_cmd '.m']) == 2
        obj.is_precomp = true;
      else
        obj.is_precomp = false;
      end
    end
    % utils --------------
% $$$     function [rot, pos, J] = get_link_state(obj, q, target_link_idx)
% $$$       [rot_joints, rot_links, pos_joints, pos_links, e, g] = Kinematics(obj.rot0, obj.pos0, q, obj.spart);
% $$$       [Bij, Bi0, P0, pm] = DiffKinematics(obj.rot0, obj.pos0, pos_links, e, g, obj.spart);
% $$$       [J0, J] = Jacob(pos_links(:, target_link_idx), obj.pos0, pos_links, P0, pm, target_link_idx, obj.spart);
% $$$       %tmp = DCM_Angles321(rot_links(:, :, target_link_idx));
% $$$       %state = [tmp; pos_links(:, target_link_idx)];
% $$$       rot = rot_links(:, :, target_link_idx);
% $$$       pos = pos_links(:, target_link_idx);
% $$$     end
    function [rot, pos] = get_link_state_by_name(obj, q, link_name)
      target_link_idx = obj.get_link_idx_by_name(link_name);
      
      [rot_joints, rot_links, pos_joints, pos_links, e, g] = Kinematics(obj.rot_base, obj.pos_base, q, obj.spart);
      [Bij, Bi0, P0, pm] = DiffKinematics(obj.rot_base, obj.pos_base, pos_links, e, g, obj.spart);
      [J0, J] = Jacob(pos_links(:, target_link_idx), obj.pos_base, pos_links, P0, pm, target_link_idx, obj.spart);
      
      rot = rot_links(:, :, target_link_idx);
      pos = pos_links(:, target_link_idx);
    end
    function [rot, pos, J] = get_link_state(obj, q)
      if obj.is_precomp == true
        robot = obj;
        eval(obj.qset_cmd);
        rot = obj.rot_base*rot;
        pos = obj.rot_base*pos + obj.pos_base;
      else
        target_link_idx = obj.get_link_idx_by_name(obj.ini.target_link_name);
        [rot_joints, rot_links, pos_joints, pos_links, e, g] = Kinematics(obj.rot_base, obj.pos_base, q, obj.spart);
        [Bij, Bi0, P0, pm] = DiffKinematics(obj.rot_base, obj.pos_base, pos_links, e, g, obj.spart);
        [J0, J] = Jacob(pos_links(:, target_link_idx), obj.pos_base, pos_links, P0, pm, target_link_idx, obj.spart);
        rot = rot_links(:, :, target_link_idx);
        pos = pos_links(:, target_link_idx);
      end
    end
    function [rot_ab, pos_ab] = get_link_state_relative(obj, q, link_name_a, link_name_b) % from link-a to link-b
      link_idx_a = obj.get_link_idx_by_name(link_name_a);
      link_idx_b = obj.get_link_idx_by_name(link_name_b);
      [rot_joints, rot_links, pos_joints, pos_links, e, g] = Kinematics(obj.rot_base, obj.pos_base, q, obj.spart);
      
      rot_link_a = rot_links(:, :, link_idx_a);
      rot_link_b = rot_links(:, :, link_idx_b);
      pos_link_a = pos_links(:, link_idx_a);
      pos_link_b = pos_links(:, link_idx_b);
      
      rot_ab = inv(rot_link_a)*rot_link_b;
      pos_ab = pos_link_b - pos_link_a;
    end
% $$$     function [rot, pos, J] = get_link_state_TX08_RHAND(obj, q)
% $$$       [rot, pos, J] = get_link_state_TX08_RHAND_sub(q);
% $$$       rot = obj.rot0*rot;
% $$$       pos = obj.rot0*pos + obj.pos0;
% $$$       %tmp = DCM_Angles321(rot);
% $$$       %state = [tmp; pos];
% $$$     end
    function [idx, id] = get_link_idx_by_name(obj, link_name)
      l = struct2table(obj.spart.links);
      idx = find(strcmp(l.name, link_name) == true);
      id = l.id(idx);
      %if idx == id
      %  error('Unexpected format!');
      %end
    end
    % ---------
    function plot_robot(obj, q)
      [rot_joints, rot_links, pos_joints, pos_links, e, g] = Kinematics(obj.rot_base, obj.pos_base, q, obj.spart);
      XX = 1; YY = 2; ZZ = 3;
      for i = 1:size(pos_joints, 2)
        plot3(pos_joints(1, i), pos_joints(2, i), pos_joints(3, i), 'r.', 'MarkerSize', 50); hold('on');
        
        scale = 0.05;
        R = rot_links(:, :, i);
        xa = R*[1; 0; 0].*scale; plot3([0, xa(XX)] + pos_joints(XX, i), [0, xa(YY)] + pos_joints(YY, i), [0, xa(ZZ)] + pos_joints(ZZ, i), 'r-', 'LineWidth', 3); hold('on');
        ya = R*[0; 1; 0].*scale; plot3([0, ya(XX)] + pos_joints(XX, i), [0, ya(YY)] + pos_joints(YY, i), [0, ya(ZZ)] + pos_joints(ZZ, i), 'g-', 'LineWidth', 3); hold('on');
        za = R*[0; 0; 1].*scale; plot3([0, za(XX)] + pos_joints(XX, i), [0, za(YY)] + pos_joints(YY, i), [0, za(ZZ)] + pos_joints(ZZ, i), 'b-', 'LineWidth', 3); hold('on');
      end
      
      for joint_id = 2:size(pos_joints, 2)
        if obj.spart.joints(joint_id).parent_link <= 0; continue; end
        parent_joint_id = obj.spart.links(obj.spart.joints(joint_id).parent_link).parent_joint;
        plot3(pos_joints(XX, [joint_id, parent_joint_id]), pos_joints(YY, [joint_id, parent_joint_id]), pos_joints(ZZ, [joint_id, parent_joint_id]), 'k-', 'LineWidth', 1);
      end
      axis('equal');
      xlabel('x [m]');
      ylabel('y [m]');
      zlabel('z [m]');
    end
  end
end



