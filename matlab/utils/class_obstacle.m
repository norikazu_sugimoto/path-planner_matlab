classdef class_obstacle
  properties
    type
    shape
    faces
  end
  methods
    % constructor -------------------------------------------
    function obj = class_obstacle(type_, shape_)
      obj.type = type_;
      obj.shape = shape_;
      obj.faces = parse_shape(type_, shape_);
    end
    % move -------------------------------------------
    function obj = move(obj, trans_, rot_)
      for n = 1:size(obj.faces, 1)
        obj.faces{n} = trans_ + rot_*obj.faces{n};
      end
    end
    % collision check --------------------------------
    function is_safe = collision_check(obj, pa, pb)
      is_safe = true;
      switch(obj.type)
        case {'brick', 'square'}
          for n = 1:size(obj.faces, 1)
            if isintersect_segment_triangle(pa, pb, obj.faces{n}(:, 1), obj.faces{n}(:, 2), obj.faces{n}(:, 3)) == true | ...
                  isintersect_segment_triangle(pa, pb, obj.faces{n}(:, 1), obj.faces{n}(:, 3), obj.faces{n}(:, 4)) == true
              is_safe = false;
              return
            end
          end
      end
    end
    % plot -------------------------------------------
    function plot(obj)
      switch(obj.type)
        case {'brick', 'square'}
          for n = 1:size(obj.faces, 1)
            h = patch(obj.faces{n}(1, :), obj.faces{n}(2, :), obj.faces{n}(3, :), [1, 1, 0]); hold('on');
            set(h,'FaceAlpha', 0.5);
          end
        otherwise
          error('error: unknown type of obstacle');
      end
    end
  end
end




function faces = parse_shape(type, shape)
  switch(type)
    case 'brick'
      p = shape(1:3);
      l = shape(4:6);
      th = shape(7:9);
      
      % faces (before rotate)
      X = 1; Y = 2; Z = 3;
      faces = {
        [
        p(X) + l(X)/2, p(Y) + l(Y)/2, p(Z) + l(Z)/2; % x+
        p(X) + l(X)/2, p(Y) + l(Y)/2, p(Z) - l(Z)/2;
        p(X) + l(X)/2, p(Y) - l(Y)/2, p(Z) - l(Z)/2;
        p(X) + l(X)/2, p(Y) - l(Y)/2, p(Z) + l(Z)/2]'
        [
        p(X) - l(X)/2, p(Y) + l(Y)/2, p(Z) + l(Z)/2; % x-
        p(X) - l(X)/2, p(Y) + l(Y)/2, p(Z) - l(Z)/2;
        p(X) - l(X)/2, p(Y) - l(Y)/2, p(Z) - l(Z)/2;
        p(X) - l(X)/2, p(Y) - l(Y)/2, p(Z) + l(Z)/2        ]'
        [
        p(X) - l(X)/2, p(Y) + l(Y)/2, p(Z) + l(Z)/2; % y+
        p(X) + l(X)/2, p(Y) + l(Y)/2, p(Z) + l(Z)/2;
        p(X) + l(X)/2, p(Y) + l(Y)/2, p(Z) - l(Z)/2;
        p(X) - l(X)/2, p(Y) + l(Y)/2, p(Z) - l(Z)/2]'
        [
        p(X) - l(X)/2, p(Y) - l(Y)/2, p(Z) + l(Z)/2; % y-
        p(X) + l(X)/2, p(Y) - l(Y)/2, p(Z) + l(Z)/2;
        p(X) + l(X)/2, p(Y) - l(Y)/2, p(Z) - l(Z)/2;
        p(X) - l(X)/2, p(Y) - l(Y)/2, p(Z) - l(Z)/2]'
        [
        p(X) - l(X)/2, p(Y) - l(Y)/2, p(Z) + l(Z)/2; % z+
        p(X) + l(X)/2, p(Y) - l(Y)/2, p(Z) + l(Z)/2;
        p(X) + l(X)/2, p(Y) + l(Y)/2, p(Z) + l(Z)/2;
        p(X) - l(X)/2, p(Y) + l(Y)/2, p(Z) + l(Z)/2]'
        [
        p(X) - l(X)/2, p(Y) - l(Y)/2, p(Z) - l(Z)/2; % z-
        p(X) + l(X)/2, p(Y) - l(Y)/2, p(Z) - l(Z)/2;
        p(X) + l(X)/2, p(Y) + l(Y)/2, p(Z) - l(Z)/2;
        p(X) - l(X)/2, p(Y) + l(Y)/2, p(Z) - l(Z)/2]'
        };
      
      % rotate faces
      R = rot_mat(th(X), 'x')*rot_mat(th(Y), 'y')*rot_mat(th(Z), 'z');
      for n = 1:size(faces, 1)
        p0 = repmat(p, 1, 4);
        faces{n} = R*(faces{n} - p0) + p0;
      end
    case 'square'
      p = shape(1:3); % position of x-y-z
      l = shape(4:5); % length of x-y
      th = shape(6:8); % rotate angle of x-y-z
      
      % faces (before rotate)
      X = 1; Y = 2; Z = 3;
      faces = {
        [
        p(X) - l(X)/2, p(Y) - l(Y)/2, p(Z);
        p(X) + l(X)/2, p(Y) - l(Y)/2, p(Z);
        p(X) + l(X)/2, p(Y) + l(Y)/2, p(Z);
        p(X) - l(X)/2, p(Y) + l(Y)/2, p(Z)]'
        };
      
      % rotate faces
      R = rot_mat(th(X), 'x')*rot_mat(th(Y), 'y')*rot_mat(th(Z), 'z');
      for n = 1:size(faces, 1)
        p0 = repmat(p, 1, 4);
        faces{n} = R*(faces{n} - p0) + p0;
      end
    otherwise
      error('error: unknown type of obstacle');
  end
end
