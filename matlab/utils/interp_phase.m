function path_interp = interp_phase(path, w)
% path: dim x samples

path_interp = [];
num = size(path, 2);
bias = 0;
for n = 1:(num - 1)
  p1 = path(:, n);
  p2 = path(:, n + 1);
  tmp = p2 - p1;
  length = norm(tmp);
  v = tmp./length;
  
  S = floor((length - bias)/w);
  t = [0:S] + bias/w;
  tmp_path = p1 + (v*t).*w;
  %plot(tmp_path(1, :), tmp_path(2, :), 'r+', 'LineWidth', 2);
  
  bias = w - (length - (bias + S*w));
  path_interp = [path_interp, tmp_path];
end


