function [ini, hash] = load_config
  urdf_filepath = 'config.ini';
  ini = ini2struct(urdf_filepath);
  ini.target_joint_names = strsplit(ini.target_joint_names);
  ini.rot_base = str2num(ini.rot_base);
  ini.pos_base = str2num(ini.pos_base);
  
  ini.reachability.bin = str2num(ini.reachability.bin);
  ini.reachability.quat_des = str2num(ini.reachability.quat_des);
  ini.reachability.rot_des = quat_DCM(ini.reachability.quat_des);
  
  hash = data_hash(ini);
  fprintf('load_config: hash is %s\n', hash);
  
  tmp_path = sprintf('autogen_%s_%s', hash, urdf_filepath);
  tmp_cmd = sprintf('cp -f %s %s', urdf_filepath, tmp_path);
  system(tmp_cmd);
end