classdef class_shelf < handle
  properties
    trans
    rot
    o
    put_point
  end
  methods
    % constructor -------------------------------------------
    function obj = class_shelf()
      obj.trans = [0; 0; 0];
      obj.rot = eye(3);
      
      hight = 1.6;
      obj.o{1} = class_obstacle('brick',  [[ 0.05/2, 0.095/2,        hight/2]';           [0.05, 0.095, hight]';     [0, 0, 0]']); % Right 
      obj.o{2} = class_obstacle('brick',  [[ 0.05/2, 0.95 - 0.058/2, hight/2]';           [0.05, 0.058, hight]';     [0, 0, 0]']); % Left
      
      draw_length = 0.236;
      % Shelf 1
      obj.o{3} = class_obstacle('square', [[ 0.45/2 - draw_length, 0.095 + 0.782/2 + 0.015/2, 0.95]';          [0.45, 0.782]'; [0, 0, 0]']);
      obj.o{4} = class_obstacle('square', [[ 0      - draw_length, 0.095 + 0.782/2 + 0.015/2, 0.95 + 0.03/2]'; [0.03, 0.782]'; [0, -90/180*pi, 0]']);
      
    end
    %
    function obj = move(obj, trans_, rot_)
    % cancel previous move
      for n = 1:length(obj.o)
        obj.o{n} = obj.o{n}.move(-obj.trans, eye(3));
        obj.o{n} = obj.o{n}.move([0; 0; 0], inv(obj.rot));
      end
      %obj.put_point = inv(obj.rot)*(-obj.trans + obj.put_point);
      
      % move
      obj.trans = trans_;
      obj.rot = rot_;
      for n = 1:length(obj.o)
        obj.o{n} = obj.o{n}.move(obj.trans, obj.rot);
      end
      %obj.put_point = obj.trans + obj.rot*obj.put_point;
    end
    % ----------------------------------------------------
    %function p = get_put_point(obj)
    %  p = obj.put_point;
    %end
    % ----------------------------------------------------
    function issafe = collision_check(obj, segs)
      for n = 1:length(obj.o)
        for m = 1:length(segs)
          issafe = obj.o{n}.collision_check(segs{m}(:, 1), segs{m}(:, 2));
          if issafe == false; return; end
        end
      end
    end
    % ----------------------------------------------------
    function plot(obj)
      for n = 1:length(obj.o)
        obj.o{n}.plot();
      end
      %plot_sphere(obj.put_point, 0.01, 'c');
      
      % plot marker
      XX = 1; YY = 2; ZZ = 3;
      scale = 0.05;
      marker_poses = {[-0.188; 0.487; 0.95], ...
                      [0; 0.950 - 0.034/2; 1.0], ...
                      [0; 0.950 - 0.034/2; 1.5]};
      marker_euler = {[0, 0, 0], [0, 90/180*pi, pi], [0, 90/180*pi, pi]};
      for n = 1:length(marker_poses);
        marker_pos = obj.rot*marker_poses{n} + obj.trans;
        marker_rot = Angles321_DCM(marker_euler{n});
        xa = obj.rot*marker_rot*[1; 0; 0].*scale; plot3([0, xa(XX)] + marker_pos(XX), [0, xa(YY)] + marker_pos(YY), [0, xa(ZZ)] + marker_pos(ZZ), 'r-', 'LineWidth', 3); hold('on');
        ya = obj.rot*marker_rot*[0; 1; 0].*scale; plot3([0, ya(XX)] + marker_pos(XX), [0, ya(YY)] + marker_pos(YY), [0, ya(ZZ)] + marker_pos(ZZ), 'g-', 'LineWidth', 3); hold('on');
        za = obj.rot*marker_rot*[0; 0; 1].*scale; plot3([0, za(XX)] + marker_pos(XX), [0, za(YY)] + marker_pos(YY), [0, za(ZZ)] + marker_pos(ZZ), 'b-', 'LineWidth', 3); hold('on');
      end
      xlabel('x [m]'); ylabel('y [m]'); zlabel('z [m]');
      grid('on');
      axis('equal');
    end
  end
end
