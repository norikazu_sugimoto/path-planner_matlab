function R = rot_mat(th, axis)

switch(axis)
  case 'x'
    R = [1, 0, 0;
         0, cos(th), -sin(th);
         0, sin(th), cos(th)];
  case 'y'
    R = [cos(th), 0, sin(th);
         0, 1, 0;
         -sin(th), 0, cos(th)];
  case 'z'
    R = [cos(th), -sin(th), 0;
         sin(th), cos(th), 0;
         0, 0, 1];
  otherwise
    error('class_obstacle::rot_mat: unknown flag of axis');
end
