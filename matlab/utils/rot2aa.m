function aa = rot2aa(rot)
  if isdiag(rot) == false % rot is not diagonal
    l = [rot(3, 2) - rot(2, 3);
         rot(1, 3) - rot(3, 1);
         rot(2, 1) - rot(1, 2)];
    l_norm = norm(l);
    aa = (atan2(l_norm, rot(1, 1) + rot(2, 2) + rot(3, 3) - 1)/l_norm)*l;
    return
  end
  
  % rot is giadonal
  if rot(1, 1) + rot(2, 2) + rot(3, 3) < 0 % diag(rot) is (1, -1, -1), (-1, 1, -1), or (-1, -1, 1)
    tmp = [rot(1, 1) + 1;
           rot(2, 2) + 1;
           rot(3, 3) + 1];
    aa = (pi/2)*tmp;
  else % diag(rot) is identify(1, 1, 1)
    aa = zeros(3, 1);
  end
end
