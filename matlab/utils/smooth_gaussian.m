function sx = smooth_gaussian(x, w)
  dim = size(x, 1);
  S = size(x, 2);
  
  
  %
  b = w*3;
  f = zeros(1, 2*b + 1);
  s = [-b:b];
  exparg = -0.5/(w*w)*(s.*s);
  exparg = exparg - max(exparg);
  tmp = exp(exparg);
  f = tmp/sum(tmp);
  
% $$$   xx = [repmat(x(:, 1), 1, b), x, repmat(x(:, end), 1, b)];
% $$$   for d = 1:dim
% $$$     for s = (b + 1):(S + b)
% $$$       s0 = s - b;
% $$$       s1 = s + b;
% $$$       
% $$$       tmp = xx(d, s0:s1).*f([s0:s1] - s + b + 1);
% $$$       sx(d, s - b) = sum(tmp);
% $$$     end
% $$$   end
  xx = [repmat(x(:, 1), 1, b), x, repmat(x(:, end), 1, b)];
  for s = (b + 1):(S + b)
    s0 = s - b;
    s1 = s + b;
    
    tmp = xx(:, s0:s1).*repmat(f([s0:s1] - s + b + 1), dim, 1);
    sx(:, s - b) = sum(tmp, 2);
  end
end


