function [path_idx, path] = astar(voxels, start_idx, goal_idx, ignore_idx)
  if nargin == 3; ignore_idx = []; end
  if start_idx == goal_idx
    path_idx = [];
    path = [];
    return
  end
  
  path_idx = [];
  path = [];
  open = [start_idx, 0, -1];
  close = [];
  isfind = false;
  
  start_time = tic();
  while isfind == false
    % openにデータがない場合はパスが見つからなかった
    if isempty(open(:, 1)); fprintf('No path to goal!!\n'); return; end
    % openなノードの中で最もコストが小さいものを選ぶ
    [~, tmp] = sort(open(:, 2));
    open = open(tmp, :);
    % ゴール判定
    if open(1, 1) == goal_idx
      fprintf('Find Goal!!(%.3s passed)\n', toc(start_time));
      %ゴールのノードをCloseの先頭に移動
      close = [open(1, :); close]; open(1, :) = [];
      isfind = true;
      break;
    end
    
    % set node, which has minimum cost in the open-list, as parent
    parent_idx = open(1, 1);
    parent_cost = open(1, 2);
    neighbor_list = voxels{parent_idx}.neighbor_list;
    for i = 1:length(neighbor_list)
      idx = neighbor_list(i);
      if isempty(find(idx == ignore_idx)) == false; continue; end
      
      if voxels{idx}.p(1) < 0.3
        amp = [10; 10; 1];
      else
        amp = [1; 1; 10];
      end
      e2 = (voxels{parent_idx}.p - voxels{idx}.p).^2;
      %disp(voxels{idx}.p);
      %amp
      %e2
      %e2.*amp
      %pause
      running_cost = sqrt(sum(e2.*amp));
      %running_cost = sqrt(sum(e2));
      cost = parent_cost + running_cost;%norm(voxels{parent_idx}.p - voxels{idx}.p);
      
      [flag, target_idx] = find_list(idx, open, close);
      if flag == 1
        if cost < open(target_idx, 2)
          open(target_idx, 2) = cost;
          open(target_idx, 3) = parent_idx;
        end
      elseif flag == 2
        if cost < close(target_idx, 2)
          close(target_idx, 3) = parent_idx;
          open = [open; close(target_idx, :)];
          close(target_idx, :) = [];
        end
      else
        open = [open; [idx, cost, parent_idx]];
      end
    end
    
    if isfind == false
      close = [close; open(1, :)];
      open(1, :) = [];
    end
  end
  
  %
  path_idx = [];
  n = 1;
  while true
    path_idx = [path_idx; close(n, 1)];
    n = find(close(:, 1) == close(n, 3));
    if close(n, 1) == start_idx
      break;
    end
  end
  path_idx = [path_idx; start_idx];
  
  %
  path = [];
  for n = 1:length(path_idx)
    path = [path, voxels{path_idx(n)}.p];
  end
  
  
  if false
    myfigure(1); clf;
    if false % plot open/close list
      plot3(voxels{start_idx}.p(1), voxels{start_idx}.p(2), voxels{start_idx}.p(3), 'r+', 'MarkerSize', 20, 'LineWidth', 3); hold('on');
      plot3(voxels{goal_idx}.p(1), voxels{goal_idx}.p(2), voxels{goal_idx}.p(3), 'ro', 'MarkerSize', 20, 'LineWidth', 3);
      for n = 1:size(open, 1)
        idx = open(n, 1);
        plot3(voxels{idx}.p(1), voxels{idx}.p(2), voxels{idx}.p(3), 'rx', 'MarkerSize', 10, 'LineWidth', 2);
      end
      for n = 1:size(close, 1)
        idx = close(n, 1);
        plot3(voxels{idx}.p(1), voxels{idx}.p(2), voxels{idx}.p(3), 'kx', 'MarkerSize', 10, 'LineWidth', 2);
      end
    end
    if true % plot all center of voxels
      for n = 1:length(voxels)
        plot3(voxels{n}.p(1), voxels{n}.p(2), voxels{n}.p(3), 'r.'); hold('on');
      end
    end
    if true
      for n = 2:length(path_idx)
        plot3([voxels{path_idx(n - 1)}.p(1), voxels{path_idx(n)}.p(1)], ...
              [voxels{path_idx(n - 1)}.p(2), voxels{path_idx(n)}.p(2)], ...
              [voxels{path_idx(n - 1)}.p(3), voxels{path_idx(n)}.p(3)], 'b-', 'LineWidth', 2); hold('on');
      end
    end
  end
end

function [flag, target_idx] = find_list(idx, open, close)
% flag:
%   1: idx is in the open-list
%   2: idx is in the close-list
%   3: idx is not in both of the open and clsoe lists
% target_idx:
%   index of open-list or close-list, or blanck
  
  % idxがopen-listにあるか？
  if isempty(open) == false
    target_idx = find(open(:, 1) == idx);
    if isempty(target_idx) == false
      flag = 1; % in open-list
      return
    end
  end
  % idxがclose-listにあるか？
  if isempty(close) == false
    target_idx = find(close(:, 1) == idx);
    if isempty(target_idx) == false
      flag = 2; % in open-list
      return
    end
  end
  % どちらにもなかった
  flag = 3;
  target_idx = -1;
  return
end