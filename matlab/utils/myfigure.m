function myfigure(n)
  try
    set(0, 'CurrentFigure', n);
  catch
    figure(n);
  end
  
end

