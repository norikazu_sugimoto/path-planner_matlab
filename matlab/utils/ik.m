function [q, error, iter] = ik(robot, cond)
  
  rot0 = eye(3); % rotation-matrix of base-link
  pos0 = zeros(3, 1); % position of base-link
  u0 = zeros(6, 1); % ?
  
  T = 5.0; % time-horizon
  dt = 0.01; % time-step
  S = floor(T/dt); % step length
  
  % copy
  q = cond.q;
  qd = cond.qd;
  target_link_id = cond.target_link_id;
  target_qid = cond.target_qid;
  state_des = cond.state_des;
  
  num = length(target_qid);
  
  ignore_qid = [];
  %state_desired = [0.0; 0; 0; 0.3; -0.1; 0.1]; % 1~3: Euler angle, 4~6: position
  We = diag([1 1 1 1 1 1]);
  sqrt_We = We^(1/2);
  for s = 1:S
    % sub-dim of Jacobian
    %target_dim = [1:6];
    Wn = 0.0001*eye(length(target_qid));
    
    % forward kinematics
    %[rot_joints, rot_links, pos_joints, pos_links, e, g] = Kinematics(rot0, pos0, q, robot.spart);
    %[Bij, Bi0, P0, pm] = DiffKinematics(rot0, pos0, pos_links, e, g, robot.spart);
    %[J0, J] = Jacob(pos_links(:, target_link_idx), pos0, pos_links, P0, pm, target_link_idx, robot.spart);
    %rot = rot_links(:, :, target_link_idx);
    %pos = pos_links(:, target_link_idx);
    [rot, pos, J] = robot.get_link_state(q);
    %DCM_Angles321(rot)
    J_sub = J(:, target_qid);
    %J_sub = J_sub*D;
    
    %
    Wq = eye(length(target_qid));
    for n = 1:length(target_qid)
      qid = target_qid(n);
      qn_min = robot.myjoints.q_min(qid);
      qn_max = robot.myjoints.q_max(qid);
      deno = 4*((qn_max - q(qid)).^2)*((q(qid) - qn_min).^2);
      G_q = ((qn_max - qn_min).^2)*(2*q(qid) - qn_max - qn_min)/(deno + eps);
      if G_q >= 0; Wq(n, n) = 1 + G_q; end
    end
    J_sub_Wq = J_sub*Wq^(-0.5);
    
    
    % current state
    state = [rot2aa(rot); pos];
    
    % error
    e = state_des - state;
    
    
    
    %if norm(vel_desired) > limit
    %  fprintf('%d, %f\n', s, norm(vel_desired))
    %  vel_desired = limit*(vel_desired./norm(vel_desired));
    %end
    
    % desired joint's vel.
    %[U, S, V] = svd(sqrt_We*J_sub);
    %[U, S, V] = svd(sqrt_We*J_sub*Wq^(-0.5));
    [U, S, V] = svd(sqrt_We*J_sub_Wq);
    
    %S_norm = S;
    %s = diag(S_norm)
    %while /max(diag(S_norm)) < 0.5
    %  for n = 1:min(size(S_norm))
    %    S_norm(n, n) = S_norm(n, n) + 0.1;
    %  end
    %  fprintf('.');
    %end
    %J_sub_norm = U*S_norm*V';
    %J_sub_norm = J_sub;
    E = 0.5*e'*We*e;
    H = V*(S'*S + min(E, 0.001)*eye(length(target_qid)) + Wn)*V';
    %H = V*(S'*S + E*eye(length(target_qid)) + Wn)*V';
    %H = J_sub'*We*J_sub;
    
    %norm(H)*norm(inv(H))
    
    % compute weighting
    %D = calc_weighting(q, qd, joints_rarm_qid, robot);
    %J_sub_norm = J_sub_norm;
    
    %
    limit = 5/180*pi;
    %size(J_sub_norm'*J_sub_norm)
    %J_sub_norm'*J_sub_norm + eye(size(J_sub_norm, 2)).*0.01
    %g = 
    %qd_sub = inv(J_sub_norm'*J_sub_norm)*J_sub_norm'*vel_desired;
    q_sub_diff = inv(H)*J_sub_Wq'*We*e;
    %qd_sub = inv(Wq)*J_sub'*inv(J_sub*inv(Wq)*J_sub' + eye(6)*0.001)*We*e./dt;
    %qd_sub = Wq^(-0.5)*qd_sub
    
    %size(J_sub_norm')
    %size(D)
    %qd_sub = inv(J_sub_norm'*D*J_sub_norm)*J_sub_norm'*D*vel_desired;
    if norm(q_sub_diff) > limit
      q_sub_diff = limit*q_sub_diff/norm(q_sub_diff);
    end
    %q_sub_diff = q_sub_diff/norm(q_sub_diff);
    q_sub_diff = q_sub_diff + 0.01*randn(length(target_qid), 1)*exp(-s*dt/0.5);
    %diag(Wq)
    
    %[q(joints_rarm_qid), qd_sub]
    qd(target_qid) = q_sub_diff./dt;
    if s == 1
      qd_filt = qd;
    else
      a = 0.5;
      qd_filt = qd*a + (1 - a)*qd_filt;
    end
    %q(joints_rarm_qid) = q(joints_rarm_qid) + q_sub_diff.*dt;
    q = q + exp(-s*dt/1.0)*qd_filt.*dt;
    [q, qd, ignore_qid] = joint_limit(q, qd, robot);
    
    
    if s == 1
      norm_ed = 0;
    else
      norm_ed = (norm(e) - pre_norm_e)/dt;
    end
    
    %fprintf('d=%3d: E=%12.8f, e=[%12.8f, %12.8f], norm_ed=%12.8f, norm(qd)=%12.8f\n', s, E, norm(e(1:3)), norm(e(4:6)), norm_ed, norm(qd_filt));
    pre_norm_e = norm(e);
    
    if (norm(e(1:3)) < 1/180*pi & norm(e(4:6)) < 0.01) | norm(qd_filt) < 1e-6
      break
    end
    
    if mod(s, 1) == 0 & false
      myfigure(100); clf;
      plot3(state_des(4), state_des(5), state_des(6), 'k+', 'LineWidth', 2, 'MarkerSize', 20); hold('on');
      robot.plot_robot(q);
      tmp1 = sprintf('%8.3f', state(1:3));
      tmp2 = sprintf('%8.3f', state(4:6));
      str = sprintf('%.2f[s], Euler=[%s], Pos=[%s]', s*dt, tmp1, tmp2);
      title(str);
      
      
      myfigure(101); clf;
      plot(robot.myjoints.q_min(target_qid), 'r+--'); hold('on');
      plot(robot.myjoints.q_max(target_qid), 'r+--');
      plot(q(target_qid), 'b-');
      drawnow();
      
    end
  end
  error = [norm(e(1:3)); norm(e(4:6))];
  iter = s;
end

function D = calc_weighting(q, qd, joints_rarm_qid, robot)
  D = ones(length(joints_rarm_qid), 1);
  margin = .1;
  b = 0.;
  % for min
  tmp = sqrt((robot.q_limit(joints_rarm_qid, 1) - q(joints_rarm_qid)).^2);
  idx_min = find(tmp < margin);% & qd(joints_rarm_qid) <= 0);
  for n = 1:length(idx_min)
    d = tmp(idx_min(n));
    y = (1 - b)/margin*d + b;
    D(idx_min(n)) = y;
  end
  
  % for max
  tmp = sqrt((robot.q_limit(joints_rarm_qid, 2) - q(joints_rarm_qid)).^2)
  idx_max = find(tmp < margin);% & qd(joints_rarm_qid) >= 0)
  for n = 1:length(idx_max)
    d = tmp(idx_max(n))
    %(1 - b)/margin
    y = ((1 - b)/margin)*d + b
    D(idx_max(n)) = y;
    %pause
  end
  
  idx = [idx_min; idx_max];
  %D(idx) = 0.0;
  D = diag(D);
  
end

function [q, qd, idx] = joint_limit(q, qd, robot)
% joint limit for min
  idx_min = find(robot.myjoints.q_min(:) > q);
  if isempty(idx_min) == false
    q(idx_min) = robot.myjoints.q_min(idx_min);
    qd(idx_min) = 0;
  end
  
  % joint limit for max
  idx_max = find(robot.myjoints.q_max(:) < q);
  if isempty(idx_max) == false
    q(idx_max) = robot.myjoints.q_max(idx_max);
    qd(idx_max) = 0;
  end
  
  idx = [idx_min; idx_max];
end