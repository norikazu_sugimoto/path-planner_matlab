function plot_sphere(p, r, color)
  
  [X, Y, Z] = sphere();
  
  X = X.*r + p(1);
  Y = Y.*r + p(2);
  Z = Z.*r + p(3);
  surf(X, Y, Z, 'FaceColor', color, 'FaceAlpha', 0.8, 'EdgeAlpha', 1);
  
end

